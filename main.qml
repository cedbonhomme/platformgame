import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Particles 2.0
import QtQuick.Window 2.1
import QtMultimedia 5.0
import "screens"

ApplicationWindow {
    id: appWindow
    visible: true
    width: Screen.width
    height: Screen.height
    visibility: Window.FullScreen
    title: qsTr("Money")

    property int defaultWidth: 800
    property int defaultHeight: 480
    property double scaleFactor: Math.min(Math.min(width, height) / defaultHeight, Math.max(width, height) / defaultWidth)
    property  var currentScreen: undefined
    property double volume: 1.0

    function setScreen(screen) {
        if (currentScreen) currentScreen.visible = false
        currentScreen = screen
        currentScreen.visible = true
        currentScreen.focus = true
    }

    Component.onCompleted: setScreen(menuScreen)

    Item {
        anchors.centerIn: parent
        //width: defaultWidth * scaleFactor
        //height: defaultHeight * scaleFactor
        width: parent.width
        height: parent.height
        rotation: parent.height > parent.width ? 90 : 0
        clip: true

        SplashScreen {
            id: splashScreen
            onFinished: setScreen(menuScreen)
        }

        MenuScreen {
            id: menuScreen
            onStartClicked: setScreen(gameScreen)
            onOptionClicked: setScreen(optionScreen)
            onBackClicked: Qt.quit()
        }

        GameScreen {
            id: gameScreen
            onBackClicked: setScreen(menuScreen)
        }

        AboutScreen {
            id: aboutScreen
            onBackClicked: setScreen(menuScreen)
        }

        OptionScreen {
            id: optionScreen
            onBackClicked: setScreen(menuScreen)
       }

    }
}


//    SoundEffect {
//        id: click1
//        source: "Data/Sounds/Effects/click1.wav"
//    }
//    SoundEffect {
//        id: click2
//        source: "Data/Sounds/Effects/click2.wav"
//    }
//    SoundEffect {
//        id: rollover1
//        source: "Data/Sounds/Effects/rollover1.wav"
//    }
//    SoundEffect {
//        id: rollover2
//        source: "Data/Sounds/Effects/rollover2.wav"
//    }
//    SoundEffect {
//        id: switch2
//        source: "Data/Sounds/Effects/switch2.wav"
//    }
//    SoundEffect {
//        id: switch3
//        source: "Data/Sounds/Effects/switch3.wav"
//    }

//    Item{
//        id:menu
//        visible: true
//        width: parent.width
//        height: parent.height
//        //state: "Menu"
//        states: [
//            State {
//                name: "Menu"
//                PropertyChanges { target: menu; visible: true; }
//            },
//            State {
//                name: "Options"
//                PropertyChanges { target: options; visible: true; }
//                PropertyChanges { target: menu; visible: false; }
//            },
//            State {
//                name: "Game"
//                PropertyChanges { target: game; visible: true; }
//                PropertyChanges { target: menu; visible: false; }
//            }
//        ]

//        ParticleSystem {
//            anchors.fill: parent

//            ImageParticle {
//                source: "Data/Textures/Items/coinGold.png"
//            }

//            Emitter {
//                anchors.fill: parent
//                size: 70
//            }
//        }

//        Text {
//            width: 178
//            height: 59
//            color: "#0a66c3"
//            text: qsTr("Get That Sweet CA$H !")
//            anchors.top: parent.top
//            anchors.topMargin: 75
//            style: Text.Raised
//            verticalAlignment: Text.AlignTop
//            horizontalAlignment: Text.AlignHCenter
//            anchors.horizontalCenter: parent.horizontalCenter
//            font.bold: true
//            font.pointSize: 69
//        }

//        ShaderEffect {
//            id: shaderEffect
//            width: 66
//            height: 94
//            x: 343
//            y: 157
//            anchors.horizontalCenterOffset: 7
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 229
//            anchors.horizontalCenter: parent.horizontalCenter

//            property variant source: image2
//            property real frequency: 20
//            property real amplitude: 0.05
//            property real time

//            NumberAnimation on time {
//                from: 0; to: Math.PI * 2
//                duration: 1000
//                loops: Animation.Infinite
//            }

//            fragmentShader:
//                "varying highp vec2 qt_TexCoord0;
//                uniform sampler2D source;
//                uniform lowp float qt_Opacity;
//                uniform highp float frequency;
//                uniform highp float amplitude;
//                uniform highp float time;
//                void main(){
//                      vec2 p= sin(time + frequency * qt_TexCoord0);
//                      gl_FragColor = texture2D(source, qt_TexCoord0 + amplitude *vec2(p.y, -p.x))* qt_Opacity;
//                           }";
//        }






//        Image {
//            id: image1
//            y: 224
//            anchors.left: parent.left
//            anchors.leftMargin: 9
//            source: "Data/Textures/Player/p3_duck.png"
//        }






//        Image {
//            id: image2
//            x: 343
//            y: 157
//            anchors.horizontalCenterOffset: 7
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 229
//            anchors.horizontalCenter: parent.horizontalCenter
//            source: "Data/Textures/Player/p2_jump.png"
//        }






//        Image {
//            id: image3
//            x: 562
//            y: 106
//            anchors.right: parent.right
//            anchors.rightMargin: 12
//            source: "Data/Textures/Player/p1_stand.png"
//        }






//        Image {
//            id: image4
//            x: 290
//            y: 402
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 8
//            source: "Data/Textures/Items/buttonRed.png"
//        }






//        Image {
//            id: image5
//            y: 402
//            anchors.left: parent.left
//            anchors.leftMargin: 8
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 8
//            source: "Data/Textures/Items/buttonRed.png"
//        }






//        Image {
//            id: image6
//            x: 562
//            y: 402
//            anchors.right: parent.right
//            anchors.rightMargin: 8
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 8
//            source: "Data/Textures/Items/buttonRed.png"
//        }












//        Button {
//            id: button3
//            x: 280
//            y: 359
//            width: 190
//            height: 45
//            text: qsTr("Quit")
//            visible: true
//            anchors.horizontalCenterOffset: 0
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 76
//            anchors.horizontalCenter: parent.horizontalCenter
//            onClicked: {
//                click2.play();
//                Qt.quit();
//            }
//        }



//        Button {
//            id: button2
//            x: 280
//            y: 330
//            width: 190
//            height: 45
//            text: qsTr("Options")
//            onClicked: {
//                click2.play();
//                menu.state = "Options";
//            }
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 127
//            anchors.horizontalCenter: parent.horizontalCenter
//        }
//        Button {
//            id: button1
//            x: 280
//            y: 257
//            width: 190
//            height: 45
//            text: "Play"
//            anchors.horizontalCenterOffset: 0
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 178
//            anchors.horizontalCenter: parent.horizontalCenter
//            onClicked: {
//                click2.play();
//                menu.state = "Game";
//            }

//            /*style: ButtonStyle {
//            background: Rectangle {
//                radius: 5
//                Image { source: control.pressed ? "Data/Textures/UI/yellow_button03.png" : "Data/Textures/UI/blue_button03.png" }
//            }*/
//        }
//    }
//    Item {
//        id:options
//        visible: false
//        width: parent.width
//        height: parent.height

//        Text {
//            width: 178
//            height: 59
//            color: "#0a66c3"
//            text: qsTr("Options")
//            anchors.top: parent.top
//            anchors.topMargin: 75
//            style: Text.Raised
//            verticalAlignment: Text.AlignTop
//            horizontalAlignment: Text.AlignHCenter
//            anchors.horizontalCenter: parent.horizontalCenter
//            font.bold: true
//            font.pointSize: 69
//        }



//        ParticleSystem {
//            anchors.fill: parent

//            ImageParticle {
//                source: "Data/Textures/Items/coinGold.png"
//            }

//            Emitter {
//                anchors.fill: parent
//                size: 70
//            }
//        }

//        ShaderEffect {
//            id: shaderEffectoption
//            width: 66
//            height: 94
//            x: 343
//            y: 157
//            anchors.horizontalCenterOffset: 7
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 229
//            anchors.horizontalCenter: parent.horizontalCenter

//            property variant source: image2option
//            property real frequency: 20
//            property real amplitude: 0.05
//            property real time

//            NumberAnimation on time {
//                from: 0; to: Math.PI * 2
//                duration: 1000
//                loops: Animation.Infinite
//            }

//            fragmentShader:
//                "varying highp vec2 qt_TexCoord0;
//                uniform sampler2D source;
//                uniform lowp float qt_Opacity;
//                uniform highp float frequency;
//                uniform highp float amplitude;
//                uniform highp float time;
//                void main(){
//                      vec2 p= sin(time + frequency * qt_TexCoord0);
//                      gl_FragColor = texture2D(source, qt_TexCoord0 + amplitude *vec2(p.y, -p.x))* qt_Opacity;
//                           }";
//        }



//        Image {
//            id: image1option
//            y: 224
//            anchors.left: parent.left
//            anchors.leftMargin: 9
//            source: "Data/Textures/Player/p3_duck.png"
//        }






//        Image {
//            id: image2option
//            x: 343
//            y: 157
//            anchors.horizontalCenterOffset: 7
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 229
//            anchors.horizontalCenter: parent.horizontalCenter
//            source: "Data/Textures/Player/p2_jump.png"
//        }






//        Image {
//            id: image3option
//            x: 562
//            y: 106
//            anchors.right: parent.right
//            anchors.rightMargin: 12
//            source: "Data/Textures/Player/p1_stand.png"
//        }






//        Image {
//            id: image4option
//            x: 290
//            y: 402
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 8
//            source: "Data/Textures/Items/buttonRed.png"
//        }






//        Image {
//            id: image5option
//            y: 402
//            anchors.left: parent.left
//            anchors.leftMargin: 8
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 8
//            source: "Data/Textures/Items/buttonRed.png"
//        }






//        Image {
//            id: image6option
//            x: 562
//            y: 402
//            anchors.right: parent.right
//            anchors.rightMargin: 8
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 8
//            source: "Data/Textures/Items/buttonRed.png"
//        }












//        Button {
//            id: button3option
//            x: 280
//            y: 415
//            width: 190
//            height: 45
//            text: qsTr("Apply")
//            visible: true
//            anchors.horizontalCenterOffset: 0
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 21
//            anchors.horizontalCenter: parent.horizontalCenter
//            onClicked: {
//                click2.play();
//                menu.state = "Menu";
//            }
//        }

//        Label {
//            id: labelthemeoption
//            x: 202
//            y: 315
//            width: 59
//            height: 23
//            text: qsTr("Theme")
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 142
//            anchors.right: comboBoxtheme.left
//            horizontalAlignment: Text.AlignHCenter
//            verticalAlignment: Text.AlignVCenter
//        }

//        ComboBox {
//            id: comboBoxtheme
//            x: 290
//            y: 315
//            anchors.horizontalCenterOffset: 6
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 142
//            anchors.horizontalCenter: parent.horizontalCenter
//            model: [ "Cool", "Super", "Awesome"]
//        }

//        ComboBox {
//            id: comboBoxlang
//            x: 290
//            y: 286
//            anchors.horizontalCenterOffset: 6
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 171
//            anchors.horizontalCenter: parent.horizontalCenter
//            model: [ "English", "English", "Not English (english)"]
//        }

//        Label {
//            id: labellangoption
//            x: 202
//            y: 286
//            width: 59
//            height: 23
//            text: qsTr("Language")
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 171
//            font.bold: false
//            horizontalAlignment: Text.AlignHCenter
//            verticalAlignment: Text.AlignVCenter
//            anchors.right: comboBoxlang.left
//        }

//        Slider {
//            id: sliderHorizontalmusic
//            x: 294
//            y: 258
//            width: 121
//            height: 22
//            anchors.horizontalCenterOffset: 8
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 200
//            anchors.horizontalCenter: parent.horizontalCenter
//        }

//        Label {
//            id: labelmusicoption
//            x: 200
//            y: 258
//            width: 63
//            height: 22
//            text: qsTr("Music")
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 200
//            anchors.right: sliderHorizontalmusic.left
//            verticalAlignment: Text.AlignVCenter
//            horizontalAlignment: Text.AlignHCenter
//        }
//    }
//    Item {
//        id:game
//        visible: false
//        width: parent.width
//        height: parent.height

//        Rectangle {
//            id:gameaera
//            width: parent.width
//            height: parent.height
//            color: "#3bc3f6"
//            anchors.verticalCenter: parent.verticalCenter
//            anchors.horizontalCenter: parent.horizontalCenter

//        }

//        AnimatedSprite {
//            id: player3
//            width: 72
//            height: 97
//            anchors.centerIn: parent
//            source: "Data/Textures/Player/p3_walk/p3_walk.png"
//            frameCount: 11
//            frameWidth: 72
//            frameHeight: 97
//        }
//        AnimatedSprite {
//            id: player1
//            width: 72
//            height: 97
//            anchors.verticalCenter: parent.verticalCenter
//            anchors.left: parent.left
//            source: "Data/Textures/Player/p1_walk/p1_walk.png"
//            frameCount: 11
//            frameWidth: 72
//            frameHeight: 97
//        }
//        AnimatedSprite {
//            id: player2
//            width: 70
//            height: 94
//            anchors.verticalCenter: parent.verticalCenter
//            anchors.right: parent.right
//            source: "Data/Textures/Player/p2_walk/p2_walk.png"
//            frameCount: 11
//            frameWidth: 70
//            frameHeight: 94
//        }

//        ParticleSystem {
//            anchors.fill: parent

//            ImageParticle {
//                source: "Data/Textures/Items/star.png"
//            }

//            Emitter {
//                anchors.fill: parent
//                size: 70
//            }
//        }

//        Text {
//            width: 178
//            height: 59
//            color: "#0a66c3"
//            text: qsTr("You WIN!")
//            anchors.top: parent.top
//            anchors.topMargin: 75
//            style: Text.Raised
//            verticalAlignment: Text.AlignTop
//            horizontalAlignment: Text.AlignHCenter
//            anchors.horizontalCenter: parent.horizontalCenter
//            font.bold: true
//            font.pointSize: 69
//        }

//        /*MouseArea {
//            anchors.fill: parent
//            acceptedButtons: Qt.LeftButton | Qt.RightButton
//            onClicked: {
//                if (!sprite.running)
//                    sprite.start()
//                if (!sprite.paused)
//                    sprite.pause()
//                if (mouse.button == Qt.RightButton) {
//                    sprite.advance(1);
//                } else {
//                    sprite.advance(-1);
//                }
//            }
//        }*/

//        Button {
//            id: button3game
//            x: 280
//            y: 415
//            width: 190
//            height: 45
//            text: qsTr("Return")
//            visible: true
//            anchors.horizontalCenterOffset: 0
//            anchors.bottom: parent.bottom
//            anchors.bottomMargin: 21
//            anchors.horizontalCenter: parent.horizontalCenter
//            onClicked: {
//                click2.play();
//                menu.state = "Menu";
//            }
//        }
//    }

