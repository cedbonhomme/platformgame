/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Layouts 1.1
import "../components"

BlankScreen {
    id: aboutScreen

    ColumnLayout {
        anchors.fill: parent
        anchors.bottomMargin: 20 * scaleFactor

        CuteHeader {
            Layout.alignment: Qt.AlignHCenter
            text: "Cute Plane"
            font.pixelSize: 100 * scaleFactor
        }

        Item { Layout.fillHeight: true }

        GridLayout {
            Layout.alignment: Qt.AlignHCenter
            columns: 2
            rows: 4
            columnSpacing: 40 * scaleFactor
            rowSpacing: 6 * scaleFactor
            flow: GridLayout.TopToBottom

            CuteLabel {
                text: "Idea & Programming"
                font.pixelSize: 30 * scaleFactor
            }
            CuteLabel {
                text: "Oleg Yadrov"
            }

            CuteLabel {
                text: "Game art"
                font.pixelSize: 30 * scaleFactor
            }
            CuteLabel {
                text: "Kenney Vleugels"
            }

            CuteLabel {
                text: "Music"
                font.pixelSize: 30 * scaleFactor
            }
            CuteLabel {
                text: "TwistBit"
            }

            CuteLabel {
                text: "Fonts"
                font.pixelSize: 30 * scaleFactor
            }
            CuteLabel {
                text: "Jan Gerner"
            }
        }

        Item { Layout.fillHeight: true }

        CuteLabel {
            Layout.alignment: Qt.AlignHCenter
            text: "Powered by Qt Quick 2.2 (Qt 5.2.1)"
            font.pixelSize: 30 * scaleFactor
        }
        CuteLabel {
            Layout.alignment: Qt.AlignHCenter
            text: "Copyright (c) 2014 Oleg Yadrov. All Rights Reserved."
            font.pixelSize: 30 * scaleFactor
        }
        CuteLabel {
            Layout.alignment: Qt.AlignHCenter
            text: "Contact: wearyinside@gmail.com"
            font.pixelSize: 30 * scaleFactor
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: aboutScreen.backClicked()
    }
}
