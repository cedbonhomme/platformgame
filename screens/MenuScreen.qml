/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2
import QtQuick.Particles 2.0

BlankScreen {
    id: menuScreen
    signal startClicked()
    signal aboutClicked()
    signal optionClicked()

    Button {
        id: quit
        x: 280
        y: 359
        width: 190
        height: 45
        text: qsTr("Quit")
        anchors.horizontalCenterOffset: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 76
        anchors.horizontalCenter: parent.horizontalCenter
        onClicked: Qt.quit()
    }
    Button {
        id: options
        x: 280
        y: 330
        width: 190
        height: 45
        text: qsTr("Options")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 127
        anchors.horizontalCenter: parent.horizontalCenter
        onClicked: optionClicked()
    }
    Button {
        id: play
        x: 280
        y: 257
        width: 190
        height: 45
        text: "Play"
        anchors.horizontalCenterOffset: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 178
        anchors.horizontalCenter: parent.horizontalCenter
        onClicked: startClicked()
    }

    ParticleSystem {
        anchors.fill: parent

        ImageParticle {
            source: "Data/Textures/Items/coinGold.png"
        }

        Emitter {
            anchors.fill: parent
            size: 70
        }
    }

    Text {
        width: 178
        height: 59
        color: "#0a66c3"
        text: qsTr("Get That Sweet CA$H !")
        anchors.top: parent.top
        anchors.topMargin: 75
        style: Text.Raised
        verticalAlignment: Text.AlignTop
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        font.bold: true
        font.pointSize: 69
    }

    ShaderEffect {
        id: shaderEffect
        width: 66
        height: 94
        x: 343
        y: 157
        anchors.horizontalCenterOffset: 7
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 229
        anchors.horizontalCenter: parent.horizontalCenter

        property variant source: image2
        property real frequency: 20
        property real amplitude: 0.05
        property real time

        NumberAnimation on time {
            from: 0; to: Math.PI * 2
            duration: 1000
            loops: Animation.Infinite
        }

        fragmentShader:
            "varying highp vec2 qt_TexCoord0;
                    uniform sampler2D source;
                    uniform lowp float qt_Opacity;
                    uniform highp float frequency;
                    uniform highp float amplitude;
                    uniform highp float time;
                    void main(){
                          vec2 p= sin(time + frequency * qt_TexCoord0);
                          gl_FragColor = texture2D(source, qt_TexCoord0 + amplitude *vec2(p.y, -p.x))* qt_Opacity;
                               }";
    }






    Image {
        id: image1
        y: 224
        anchors.left: parent.left
        anchors.leftMargin: 9
        source: "../Data/Textures/Player/p3_duck.png"
    }






    Image {
        id: image2
        x: 343
        y: 157
        anchors.horizontalCenterOffset: 7
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 229
        anchors.horizontalCenter: parent.horizontalCenter
        source: "../Data/Textures/Player/p2_jump.png"
    }






    Image {
        id: image3
        x: 562
        y: 106
        anchors.right: parent.right
        anchors.rightMargin: 12
        source: "../Data/Textures/Player/p1_stand.png"
    }






    Image {
        id: image4
        x: 290
        y: 402
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        source: "../Data/Textures/Items/buttonRed.png"
    }






    Image {
        id: image5
        y: 402
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        source: "../Data/Textures/Items/buttonRed.png"
    }






    Image {
        id: image6
        x: 562
        y: 402
        anchors.right: parent.right
        anchors.rightMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        source: "../Data/Textures/Items/buttonRed.png"
    }
}
