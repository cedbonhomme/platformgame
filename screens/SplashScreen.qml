/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Layouts 1.1
import "../components"
import "../scripts/Functions.js" as Functions

BlankScreen {
    id: splashScreen
    property var titles: ["Oleg Yadrov Presents", "Qt Everywhere", "QML Is Awesome", "Experience Freedom!"]
    signal finished()
    onVisibleChanged: if (visible) planeAnimation.start()

    CuteLabel {
        anchors.horizontalCenter: parent.horizontalCenter
        y: 200 * scaleFactor
        text: settings.highscore === 0 ? titles[0] : titles[Functions.randomInt(0, titles.length - 1)]
    }

    VectorAnimation {
        id: planeImage
        sourcePath: "../images/plane/" + planeColor
        width: 87 * scaleFactor
        height: 73 * scaleFactor
        x: -width
        y: 200 * scaleFactor
        running: true
        frameDuration: 100
        frameCount: 3
        antialiasing: true
        Behavior on rotation { NumberAnimation { duration: 100 } }

        NumberAnimation on x {
            id: planeAnimation
            from: -planeImage.width
            to: splashScreen.width * 2
            duration: 2800
            loops: 1
            running: true
            onStopped: splashScreen.finished()
        }
    }
}
