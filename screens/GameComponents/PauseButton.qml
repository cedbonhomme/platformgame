import QtQuick 2.2

Item {
    id: pauseButton
    width: 80 * scaleFactor
    height: 80 * scaleFactor
    scale: mouseArea.pressed ? 0.9 : 1.0
    signal clicked()
    Behavior on scale { NumberAnimation { duration: 100 } }

    Rectangle {
        anchors.fill: parent
        color: Qt.rgba(0, 0, 0, 0)
        border.color: "black"
        border.width: 5 * scaleFactor
        antialiasing: true
        radius: height / 2

        Row {
            anchors.centerIn: parent
            spacing: 6 * scaleFactor

            Rectangle {
                width: 10 * scaleFactor
                height: 34 * scaleFactor
                color: "black"
                radius: width / 2
            }

            Rectangle {
                width: 10 * scaleFactor
                height: 34 * scaleFactor
                color: "black"
                radius: width / 2
            }
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: pauseButton.clicked()
    }
}
