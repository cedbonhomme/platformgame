/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2
import Box2D 1.1
import "../../components"
//import "../../scripts/Functions.js" as Functions

Body {
    id: player
    width: 72
    height: 97
    bodyType: Body.Dynamic
    property alias running: playerImage.running
    signal crash()

    fixtures: [
        Polygon {
            restitution: 0
            categories: Box.Category1
            collidesWith: Box.Category2 | Box.Category3
            onBeginContact: player.crash()
            vertices: [
                Qt.point(23 * scaleFactor, 17 * scaleFactor),
                Qt.point(18 * scaleFactor, 13 * scaleFactor),
                Qt.point(8 * scaleFactor, 11 * scaleFactor),
                Qt.point(3 * scaleFactor, 12 * scaleFactor),
                Qt.point(0 * scaleFactor, 16 * scaleFactor),
                Qt.point(1 * scaleFactor, 24 * scaleFactor),
                Qt.point(5 * scaleFactor, 31 * scaleFactor),
                Qt.point(11 * scaleFactor, 33 * scaleFactor)
            ]
        },
        Polygon {
            restitution: 0
            categories: Box.Category1
            collidesWith: Box.Category2 | Box.Category3
            onBeginContact: player.crash()
            vertices: [
                Qt.point(11 * scaleFactor, 33 * scaleFactor),
                Qt.point(21 * scaleFactor, 45 * scaleFactor),
                Qt.point(28 * scaleFactor, 17 * scaleFactor),
                Qt.point(23 * scaleFactor, 17 * scaleFactor)
            ]
        },
        Polygon {
            restitution: 0
            categories: Box.Category1
            collidesWith: Box.Category2 | Box.Category3
            onBeginContact: player.crash()
            vertices: [
                Qt.point(22 * scaleFactor, 49 * scaleFactor),
                Qt.point(19 * scaleFactor, 52 * scaleFactor),
                Qt.point(19 * scaleFactor, 60 * scaleFactor),
                Qt.point(22 * scaleFactor, 64 * scaleFactor),
                Qt.point(28 * scaleFactor, 64 * scaleFactor),
                Qt.point(33 * scaleFactor, 60 * scaleFactor)
            ]
        },
        Polygon {
            restitution: 0
            categories: Box.Category1
            collidesWith: Box.Category2 | Box.Category3
            onBeginContact: player.crash()
            vertices: [
                Qt.point(48 * scaleFactor, 65 * scaleFactor),
                Qt.point(53 * scaleFactor, 71 * scaleFactor),
                Qt.point(58 * scaleFactor, 72 * scaleFactor),
                Qt.point(64 * scaleFactor, 70 * scaleFactor),
                Qt.point(68 * scaleFactor, 64 * scaleFactor)
            ]
        },
        Polygon {
            restitution: 0
            categories: Box.Category1
            collidesWith: Box.Category2 | Box.Category3
            onBeginContact: player.crash()
            vertices: [
                Qt.point(78 * scaleFactor, 56 * scaleFactor),
                Qt.point(83 * scaleFactor, 60 * scaleFactor),
                Qt.point(86 * scaleFactor, 56 * scaleFactor),
                Qt.point(86 * scaleFactor, 27 * scaleFactor),
                Qt.point(82 * scaleFactor, 23 * scaleFactor),
                Qt.point(78 * scaleFactor, 26 * scaleFactor)
            ]
        },
        Polygon {
            restitution: 0
            categories: Box.Category1
            collidesWith: Box.Category2 | Box.Category3
            onBeginContact: player.crash()
            vertices: [
                Qt.point(65 * scaleFactor, 13 * scaleFactor),
                Qt.point(72 * scaleFactor, 12 * scaleFactor),
                Qt.point(75 * scaleFactor, 7 * scaleFactor),
                Qt.point(70 * scaleFactor, 0 * scaleFactor)
            ]
        },
        Polygon {
            restitution: 0
            categories: Box.Category1
            collidesWith: Box.Category2 | Box.Category3
            onBeginContact: player.crash()
            vertices: [
                Qt.point(70 * scaleFactor, 0 * scaleFactor),
                Qt.point(57 * scaleFactor, 0 * scaleFactor),
                Qt.point(30 * scaleFactor, 14 * scaleFactor),
                Qt.point(63 * scaleFactor, 17 * scaleFactor),
                Qt.point(65 * scaleFactor, 13 * scaleFactor)
            ]
        },
        Polygon {
            restitution: 0
            categories: Box.Category1
            collidesWith: Box.Category2 | Box.Category3
            onBeginContact: player.crash()
            vertices: [
                Qt.point(30 * scaleFactor, 14 * scaleFactor),
                Qt.point(28 * scaleFactor, 17 * scaleFactor),
                Qt.point(21 * scaleFactor, 45 * scaleFactor),
                Qt.point(22 * scaleFactor, 49 * scaleFactor),
                Qt.point(33 * scaleFactor, 60 * scaleFactor),
                Qt.point(48 * scaleFactor, 65 * scaleFactor),
                Qt.point(68 * scaleFactor, 64 * scaleFactor)
            ]
        },
        Polygon {
            restitution: 0
            categories: Box.Category1
            collidesWith: Box.Category2 | Box.Category3
            onBeginContact: player.crash()
            vertices: [
                Qt.point(68 * scaleFactor, 64 * scaleFactor),
                Qt.point(74 * scaleFactor, 62 * scaleFactor),
                Qt.point(78 * scaleFactor, 56 * scaleFactor),
                Qt.point(78 * scaleFactor, 26 * scaleFactor),
                Qt.point(71 * scaleFactor, 18 * scaleFactor),
                Qt.point(63 * scaleFactor, 17 * scaleFactor),
                Qt.point(30 * scaleFactor, 14 * scaleFactor)
            ]
        },
        Polygon {
            restitution: 0
            categories: Box.Category1
            collidesWith: Box.Category2 | Box.Category3
            onBeginContact: player.crash()
            vertices: [
                Qt.point(57 * scaleFactor, 0 * scaleFactor),
                Qt.point(25 * scaleFactor, 0 * scaleFactor),
                Qt.point(22 * scaleFactor, 3 * scaleFactor),
                Qt.point(21 * scaleFactor, 7 * scaleFactor),
                Qt.point(25 * scaleFactor, 12 * scaleFactor),
                Qt.point(30 * scaleFactor, 14 * scaleFactor)
            ]
        }
    ]

    onXChanged: playerImage.rotation = player.linearVelocity.y / scaleFactor * 0.06

    /*VectorAnimation {
        id: playerImage
        sourcePath: "../images/plane/" + planeColor
        width: 87 * scaleFactor
        height: 73 * scaleFactor
        running: true
        frameDuration: 100
        frameCount: 3
        antialiasing: true
        Behavior on rotation { NumberAnimation { duration: 100 } }
    }*/

    AnimatedSprite {
        id: playerImage
        width: 72
        height: 97
        anchors.centerIn: parent
        source: "../../Data/Textures/Player/p3_walk/p3_walk.png"
        frameCount: 11
        frameWidth: 72
        frameHeight: 97
        Behavior on rotation { NumberAnimation { duration: 100 } }
    }

    function resetRotation() {
        playerImage.rotation = 0
    }
}
