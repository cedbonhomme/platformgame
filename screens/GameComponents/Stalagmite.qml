/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2
import Box2D 1.1
import "../../components"

Body {
    width: 108 * scaleFactor
    height: 238 * scaleFactor
    bodyType: Body.Static

    fixtures: [
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(95 * scaleFactor, 173 * scaleFactor),
                Qt.point(87 * scaleFactor, 103 * scaleFactor),
                Qt.point(82 * scaleFactor, 102 * scaleFactor),
                Qt.point(37 * scaleFactor, 122 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(37 * scaleFactor, 122 * scaleFactor),
                Qt.point(29 * scaleFactor, 128 * scaleFactor),
                Qt.point(0 * scaleFactor, 238 * scaleFactor),
                Qt.point(108 * scaleFactor, 238 * scaleFactor),
                Qt.point(102 * scaleFactor, 180 * scaleFactor),
                Qt.point(95 * scaleFactor, 173 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(82 * scaleFactor, 102 * scaleFactor),
                Qt.point(71 * scaleFactor, 0 * scaleFactor),
                Qt.point(63 * scaleFactor, 0 * scaleFactor),
                Qt.point(37 * scaleFactor, 122 * scaleFactor)
            ]
        }
    ]

    VectorSprite {
        width: 108 * scaleFactor
        height: 238 * scaleFactor
        source: "../images/stalagmite.svg"
    }
}
