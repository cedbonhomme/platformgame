/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2
import Box2D 1.1
import "../../components"

Body {
    width: 108 * scaleFactor
    height: 238 * scaleFactor
    bodyType: Body.Static

    fixtures: [
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(37 * scaleFactor, 115 * scaleFactor),
                Qt.point(62 * scaleFactor, 238 * scaleFactor),
                Qt.point(70 * scaleFactor, 238 * scaleFactor),
                Qt.point(82 * scaleFactor, 136 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(94 * scaleFactor, 65 * scaleFactor),
                Qt.point(100 * scaleFactor, 60 * scaleFactor),
                Qt.point(108 * scaleFactor, 0 * scaleFactor),
                Qt.point(0 * scaleFactor, 0 * scaleFactor),
                Qt.point(28 * scaleFactor, 107 * scaleFactor),
                Qt.point(37 * scaleFactor, 115 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(37 * scaleFactor, 115 * scaleFactor),
                Qt.point(82 * scaleFactor, 136 * scaleFactor),
                Qt.point(86 * scaleFactor, 134 * scaleFactor),
                Qt.point(94 * scaleFactor, 65 * scaleFactor)
            ]
        }
    ]

    VectorSprite {
        width: 108 * scaleFactor
        height: 238 * scaleFactor
        source: "../images/stalactite.svg"
    }
}
