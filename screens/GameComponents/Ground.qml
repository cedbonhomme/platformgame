/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2
import Box2D 1.1
import "../../components"

Body {
    width: 809 * scaleFactor
    height: 71 * scaleFactor
    bodyType: Body.Static

    fixtures: [
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(309 * scaleFactor, 27 * scaleFactor),
                Qt.point(251 * scaleFactor, 4 * scaleFactor),
                Qt.point(157 * scaleFactor, 7 * scaleFactor),
                Qt.point(133 * scaleFactor, 34 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(42 * scaleFactor, 42 * scaleFactor),
                Qt.point(34 * scaleFactor, 34 * scaleFactor),
                Qt.point(0 * scaleFactor, 35 * scaleFactor),
                Qt.point(0 * scaleFactor, 71 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(0 * scaleFactor, 71 * scaleFactor),
                Qt.point(146 * scaleFactor, 71 * scaleFactor),
                Qt.point(91 * scaleFactor, 45 * scaleFactor),
                Qt.point(42 * scaleFactor, 42 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(146 * scaleFactor, 71 * scaleFactor),
                Qt.point(470 * scaleFactor, 44 * scaleFactor),
                Qt.point(349 * scaleFactor, 28 * scaleFactor),
                Qt.point(309 * scaleFactor, 27 * scaleFactor),
                Qt.point(133 * scaleFactor, 34 * scaleFactor),
                Qt.point(91 * scaleFactor, 45 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(470 * scaleFactor, 44 * scaleFactor),
                Qt.point(437 * scaleFactor, 12 * scaleFactor),
                Qt.point(372 * scaleFactor, 13 * scaleFactor),
                Qt.point(349 * scaleFactor, 28 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(146 * scaleFactor, 71 * scaleFactor),
                Qt.point(532 * scaleFactor, 55 * scaleFactor),
                Qt.point(508 * scaleFactor, 43 * scaleFactor),
                Qt.point(470 * scaleFactor, 44 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(146 * scaleFactor, 71 * scaleFactor),
                Qt.point(809 * scaleFactor, 71 * scaleFactor),
                Qt.point(573 * scaleFactor, 55 * scaleFactor),
                Qt.point(532 * scaleFactor, 55 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(809 * scaleFactor, 71 * scaleFactor),
                Qt.point(809 * scaleFactor, 35 * scaleFactor),
                Qt.point(763 * scaleFactor, 25 * scaleFactor),
                Qt.point(636 * scaleFactor, 24 * scaleFactor),
                Qt.point(599 * scaleFactor, 27 * scaleFactor),
                Qt.point(573 * scaleFactor, 55 * scaleFactor)
            ]
        },
        Polygon {
            categories: Box.Category2
            collidesWith: Box.Category1
            vertices: [
                Qt.point(763 * scaleFactor, 25 * scaleFactor),
                Qt.point(744 * scaleFactor, 0 * scaleFactor),
                Qt.point(658 * scaleFactor, 4 * scaleFactor),
                Qt.point(636 * scaleFactor, 24 * scaleFactor)
            ]
        }
    ]

    VectorSprite {
        width: 809 * scaleFactor
        height: 71 * scaleFactor
        source: "../images/ground.svg"
    }
}
