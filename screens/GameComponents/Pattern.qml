/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Particles 2.0
import "../../scripts/Functions.js" as Functions

Item {
    id: pattern
    width: 809 * scaleFactor
    height: defaultHeight * scaleFactor
    Component.onCompleted: update()

    Ceiling {
        id: ceiling
        anchors.bottom: parent.top
        parent: pattern.parent
    }

    Ground {
        id: ground
        anchors.bottom: parent.bottom
        parent: pattern.parent
    }

    ParticleSystem {
        anchors.fill: parent

        ImageParticle {
            source: "../../Data/Textures/Items/coinGold.png"
        }

        Emitter {
            anchors.fill: parent
            size: 70
        }
    }

    /*Stalactite {
        id: stalactiteA
        parent: pattern.parent
        anchors.top: parent.top
    }

    Stalagmite {
        id: stalagmiteA
        parent: pattern.parent
        anchors.bottom: parent.bottom
    }

    Stalactite {
        id: stalactiteB
        parent: pattern.parent
        anchors.top: parent.top
    }

    Stalagmite {
        id: stalagmiteB
        parent: pattern.parent
        anchors.bottom: parent.bottom
    }*/

    function update() {
        ceiling.x = pattern.x
        ground.x = pattern.x

        /*if (pattern.x ===0) {
            stalactiteA.x = 0
            stalactiteA.anchors.topMargin = 0

            stalactiteB.x = 500 * scaleFactor
            stalactiteB.anchors.topMargin = -60 * scaleFactor

            stalagmiteA.x = 240 * scaleFactor
            stalagmiteA.anchors.bottomMargin = -40 * scaleFactor

            stalagmiteB.x = 700 * scaleFactor
            stalagmiteB.anchors.bottomMargin = -40 * scaleFactor
        } else {
            var xVariance = 100
            var yMax = 20
            var yMin = 100
            stalactiteA.x = pattern.x + Functions.randomInt(0, xVariance * scaleFactor)
            stalactiteA.anchors.topMargin = Functions.randomInt(-yMin * scaleFactor, -yMax * scaleFactor)

            stalagmiteA.x = pattern.x + 200 * scaleFactor + Functions.randomInt(0, xVariance * scaleFactor)
            stalagmiteA.anchors.bottomMargin = Functions.randomInt(-yMin * scaleFactor, -yMax * scaleFactor)

            stalactiteB.x = pattern.x + 400 * scaleFactor + Functions.randomInt(0, xVariance * scaleFactor)
            stalactiteB.anchors.topMargin = Functions.randomInt(-yMin * scaleFactor, -yMax * scaleFactor)

            stalagmiteB.x = pattern.x + 600 * scaleFactor + Functions.randomInt(0, xVariance * scaleFactor)
            stalagmiteB.anchors.bottomMargin = Functions.randomInt(-yMin * scaleFactor, -yMax * scaleFactor)
        }*/
    }
}
