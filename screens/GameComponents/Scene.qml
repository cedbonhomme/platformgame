/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2
import QtMultimedia 5.0
import Box2D 1.1
import QtQuick.Particles 2.0
import "../../components"
//import "../../scripts/Functions.js" as Functions

Item {
    id: scene
    anchors.fill: parent
    property int nextUpdate: 908 * scaleFactor
    property int score: 0
    states: [
        State { name: "start" },
        State { name: "action" },
        State { name: "pause" },
        State { name: "gameover" }
    ]
    state: "start"
    signal restartClicked()
    signal backClicked()

    Flickable {
        anchors.fill: parent
        interactive: false
        contentX: player.x - 100 * scaleFactor

        World {
            id: world
            anchors.fill: parent
            gravity: Qt.point(0, 20 * scaleFactor)
            running: scene.state === "action" || scene.state === "gameover"
            onStepped: updateWorld()

            Player {
                id: player
                x: 100 * scaleFactor
                y: 200 * scaleFactor
                running: scene.state === "action"
                /*onCrash: {
                    if (!crashSound.playing) crashSound.play()
                    if (scene.state === "action") gameOver()
                }*/
            }

            Pattern {
                id: patternA
                anchors.leftMargin: -2
            }

            Pattern {
                id: patternB
                anchors.leftMargin: -2
                anchors.left: patternA.right
            }

            DebugDraw {
                anchors.fill: parent
                world: world
                visible: false
                opacity: 0.5
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        onPressed: {
            switch (scene.state) {
            case "start":
                scene.state = "action"
                player.linearVelocity.x = 220 * scaleFactor
                player.linearVelocity.y = -350 * scaleFactor
                tapSound.play()
                break;
            case "action":
                player.linearVelocity.x = 220 * scaleFactor + player.x / scaleFactor * 0.02
                player.linearVelocity.y = -350 * scaleFactor
                tapSound.play()
                break;
            case "pause":
                break;
            case "gameover":
                break;
            }
        }
    }

    CuteLabel {
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 20 * scaleFactor
        text: "Score: " + scene.score
        visible: scene.state === "start" || scene.state === "action"
    }

    PauseButton {
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 20 * scaleFactor
        visible: scene.state === "start" || scene.state === "action"
        onClicked: scene.state = "pause"
    }

    PauseLayer {
        id: pauseLayer
        score: scene.score
        visible: scene.state === "pause"
        onContinueClicked: scene.score === 0 ? scene.state = "start" : scene.state = "action"
        onMenuClicked: {
            checkHighscore()
            scene.backClicked()
        }
    }

    GameOverLayer {
        id: gameOverLayer
        visible: scene.state === "gameover"
        onRestartClicked: scene.restartClicked()
        onMenuClicked: scene.backClicked()
    }

    SoundEffect {
        id: tapSound
        volume: 0.6 * appWindow.volume
        source: "../../sounds/tap.wav"
    }

    SoundEffect {
        id: crashSound
        volume: 0.8 * appWindow.volume
        source: "../../sounds/crash.wav"
    }

    function updateWorld() {
        if (scene.state === "action") score = Math.max(score, player.x / scaleFactor * 0.01)
        if (player.x > nextUpdate) {
            nextUpdate += 808 * scaleFactor
            var firstPattern = patternA
            var secondPattern = patternB
            if (patternB.x < patternA.x) {
                firstPattern = patternB
                secondPattern = patternA
            }
            secondPattern.anchors.left = undefined
            firstPattern.anchors.left = secondPattern.right
            firstPattern.update()
        }
        if (player.y < 0) player.y = height
        if (player.y > height) player.y = 0
    }

    function checkHighscore() {
        if (score > settings.highscore) settings.highscore = score
    }

    function gameOver() {
        scene.state = "gameover"
        gameOverLayer.score = score
        checkHighscore()
    }
}
