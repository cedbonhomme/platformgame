import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2
import QtQuick.Particles 2.0

BlankScreen {
    id: optionScreen

    Text {
        width: 178
        height: 59
        color: "#0a66c3"
        text: qsTr("Options")
        anchors.top: parent.top
        anchors.topMargin: 75
        style: Text.Raised
        verticalAlignment: Text.AlignTop
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        font.bold: true
        font.pointSize: 69
    }

    ParticleSystem {
        anchors.fill: parent

        ImageParticle {
            source: "../Data/Textures/Items/coinGold.png"
        }

        Emitter {
            anchors.fill: parent
            size: 70
        }
    }

    ShaderEffect {
        id: shaderEffectoption
        width: 66
        height: 94
        x: 343
        y: 157
        anchors.horizontalCenterOffset: 7
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 229
        anchors.horizontalCenter: parent.horizontalCenter

        property variant source: image2option
        property real frequency: 20
        property real amplitude: 0.05
        property real time

        NumberAnimation on time {
            from: 0; to: Math.PI * 2
            duration: 1000
            loops: Animation.Infinite
        }

        fragmentShader:
            "varying highp vec2 qt_TexCoord0;
                    uniform sampler2D source;
                    uniform lowp float qt_Opacity;
                    uniform highp float frequency;
                    uniform highp float amplitude;
                    uniform highp float time;
                    void main(){
                          vec2 p= sin(time + frequency * qt_TexCoord0);
                          gl_FragColor = texture2D(source, qt_TexCoord0 + amplitude *vec2(p.y, -p.x))* qt_Opacity;
                               }";
    }



    Image {
        id: image1option
        y: 224
        anchors.left: parent.left
        anchors.leftMargin: 9
        source: "../Data/Textures/Player/p3_duck.png"
    }






    Image {
        id: image2option
        x: 343
        y: 157
        anchors.horizontalCenterOffset: 7
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 229
        anchors.horizontalCenter: parent.horizontalCenter
        source: "../Data/Textures/Player/p2_jump.png"
    }






    Image {
        id: image3option
        x: 562
        y: 106
        anchors.right: parent.right
        anchors.rightMargin: 12
        source: "../Data/Textures/Player/p1_stand.png"
    }






    Image {
        id: image4option
        x: 290
        y: 402
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        source: "../Data/Textures/Items/buttonRed.png"
    }






    Image {
        id: image5option
        y: 402
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        source: "../Data/Textures/Items/buttonRed.png"
    }






    Image {
        id: image6option
        x: 562
        y: 402
        anchors.right: parent.right
        anchors.rightMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        source: "../Data/Textures/Items/buttonRed.png"
    }


    Button {
        id: button3option
        x: 280
        y: 415
        width: 190
        height: 45
        text: qsTr("Apply")
        anchors.horizontalCenterOffset: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 21
        anchors.horizontalCenter: parent.horizontalCenter
        onClicked: backClicked()
    }

    Label {
        id: labelthemeoption
        x: 202
        y: 315
        width: 59
        height: 23
        text: qsTr("Theme")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 142
        anchors.right: comboBoxtheme.left
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    ComboBox {
        id: comboBoxtheme
        x: 290
        y: 315
        anchors.horizontalCenterOffset: 6
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 142
        anchors.horizontalCenter: parent.horizontalCenter
        model: [ "Cool", "Super", "Awesome"]
    }

    ComboBox {
        id: comboBoxlang
        x: 290
        y: 286
        anchors.horizontalCenterOffset: 6
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 171
        anchors.horizontalCenter: parent.horizontalCenter
        model: [ "English", "English", "Not English (english)"]
    }

    Label {
        id: labellangoption
        x: 202
        y: 286
        width: 59
        height: 23
        text: qsTr("Language")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 171
        font.bold: false
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.right: comboBoxlang.left
    }

    Slider {
        id: sliderHorizontalmusic
        x: 294
        y: 258
        width: 121
        height: 22
        anchors.horizontalCenterOffset: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 200
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Label {
        id: labelmusicoption
        x: 200
        y: 258
        width: 63
        height: 22
        text: qsTr("Music")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 200
        anchors.right: sliderHorizontalmusic.left
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }
}
