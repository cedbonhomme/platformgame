/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2
import QtMultimedia 5.0
import Box2D 1.1


BlankScreen {
    id: gameScreen
    anchors.fill: parent
    layer.enabled: true
    onVisibleChanged: loader.active = visible

    Loader {
        id: loader
        anchors.fill: parent
        active: false
        source: "GameComponents/Scene.qml"
        onLoaded: {
            item.onBackClicked.connect(gameScreen.backClicked)
            item.onRestartClicked.connect(gameScreen.restart)
        }
    }

    function restart() {
        loader.active = false
        loader.active = true
    }
}

