/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2

Item {
    id: vectorAnimation
    property string sourcePath: ""
    property int currentFrame: 0
    property int frameCount: 1
    property int frameDuration: 1000
    property bool running: false
    property bool antialiasing: false

    Image {
        source: vectorAnimation.sourcePath + vectorAnimation.currentFrame + ".svg"
        sourceSize.width: vectorAnimation.width
        sourceSize.height: vectorAnimation.height
        antialiasing: vectorAnimation.antialiasing
    }

    Timer {
        interval: vectorAnimation.frameDuration
        running: vectorAnimation.running
        repeat: true
        onTriggered: {
            var nextFrame = currentFrame + 1
            if (nextFrame === vectorAnimation.frameCount) nextFrame = 0
            currentFrame = nextFrame
        }
    }
}
