/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2

Item {
    id: cuteButton
    width: 240 * scaleFactor
    height: 80 * scaleFactor
    scale: mouseArea.pressed ? 0.9 : 1.0
    property alias text: buttonText.text
    signal clicked()
    Behavior on scale { NumberAnimation { duration: 100 } }

    Rectangle {
        anchors.fill: parent
        color: Qt.rgba(0, 0, 0, 0)
        border.color: "black"
        border.width: 5 * scaleFactor
        antialiasing: true
        radius: height / 2
    }

    CuteLabel {
        id: buttonText
        anchors.centerIn: parent
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: cuteButton.clicked()
    }
}
