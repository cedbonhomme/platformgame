/****************************************************************************
**
** Copyright (C) 2014 Oleg Yadrov.
** Contact: wearyinside@gmail.com
**
** This file is part of Cute Plane.
**
** Cute Plane is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Cute Plane is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Cute Plane. If not, see http://www.gnu.org/licenses/.
**
****************************************************************************/

import QtQuick 2.2

Item {
    id: vectorSprite
    property string source: ""
    property bool proportional: true
    property bool antialiasing: false

    Image {
        width: proportional ? implicitWidth : vectorSprite.width
        height: proportional ? implicitHeight : vectorSprite.height
        source: vectorSprite.source
        sourceSize.width: vectorSprite.width
        sourceSize.height: vectorSprite.height
        antialiasing: vectorSprite.antialiasing
    }
}
