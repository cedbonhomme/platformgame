## YET ANOTHER PLATFORM GAME

## Wiki:
#### What is this project about:
###### 
    I wanted both to sharpen my skills in video game development and try the awesome Qt library.
    A platform game is for me the gameplay that will let me achieve those priorities without tearing off my hair.

#### Dependencies:
###### 
    In order to build this project please upgrade to the lattest version of Qt 5.
    
#### What is it?:
###### 
    For now it's not really much, but stay tuned for an amazing gaming experience.
    During E3 they promoted games for mid 2015.
    I in the other hand can guarantee you my game will be ready for September !
    
#### AUTHOR
- A project by Cédric Bonhomme.

#### Credit
